Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: print-manager
Source: git://anongit.kde.org/print-manager

Files: *
Copyright: 1997-2007 by Easy Software Products
           2007-2009 Apple Inc
           2010-2018 Daniel Nicoletti <dantti12@gmail.com>
           2012 Harald Sitter <sitter@kde.org>
           2015 Jan Grulich  <jgrulich@redhat.com>
           2014 Lukáš Tinkl <ltinkl@redhat.com>
           2014 Abdalrahim G. Fakhouri <abdilra7eem@yahoo.com>
           2015 Safa Alfulaij <safa1996alfulaij@gmail.com>
           2014 Svetoslav Stefanov <svetlisashkov@yahoo.com>
           2012 Samir Ribic <megaribi@epn.ba>
           2013-2018 Vit Pelcak <vit@pelcak.org>
           2012, 2013 Tomáš Chvátal <tomas.chvatal@gmail.com>
           2012-2015 Martin Schlander <mschlander@opensuse.org>
           2013-2018 Burkhard Lück <lueck@hube-lueck.de>
           2013 Frederik Schwarzer <schwarzer@kde.org>
           2012, 2013 Antonis Geralis <capoiosct@gmail.com>
           2013 Stelios <sstavra@gmail.com>
           2015 Dimitris Kardarakos <dimkard@gmail.com>
           2018 Long Run <pvidalis@gmail.com>
           2012, 2013 Αντώνης Γέραλης <capoiosct@gmail.com>
           2014-2018 Steve Allewell <steve.allewell@gmail.com>
           2013-2018 Eloy Cuadra <ecuadra@eloihr.net>
           2013 Javier Viñal <fjvinal@gmail.com>
           2012-2016 Marek Laane <bald@smail.ee>
           2013, 2017 Iñigo Salvador Azurmendi <xalba@euskalnet.net>
           2013, 2017 Lasse Liehu <lasse.liehu@gmail.com>
           2018 Tommi Nieminen <translator@legisign.org>
           2012 Jiri Grönroos <jiri.gronroos+kde@iki.fi>
           2013 xavier <ktranslator31@yahoo.fr>
           2015 Thomas Vergnaud <thomas.vergnaud@gmx.fr>
           2018 Simon Depiets <sdepiets@gmail.com>
           2012, 2013 Joëlle Cornavin <jcorn@free.fr>
           2017 Vincent Pinon <vpinon@kde.org>
           2018 Simon Depiets <sdepiets@gmail.com>
           2012 Kevin Scannell <kscanne@gmail.com>
           2013, 2014 Marce Villarino <mvillarino@kde-espana.es>
           2013, 2015 Adrian Chaves Fernandez <adriyetichaves@gmail.com>
           2018, 2019 Adrián Chaves (Gallaecio) <adrian@chaves.io>
           2012, 2015 Kristóf Kiszel <ulysses@kubuntu.org>
           2012, 2013 Balázs Úr <urbalazs@gmail.com>
           2013, 2018 Giovanni Sora <g.sora@tiscali.it>
           2013-2018 Luigi Toscano <luigi.toscano@tiscali.it>
           2018 Japanese KDE translation team <kde-jp@kde.org>
           2013 Sairan Kikkarin <sairan@computer.org>
           2012 Khoem Sokhem <khoemsokhem@khmeros.info>
           2012 sutha <sutha@khmeros.info>
           2013, 2015 Shinjo Park <kde@peremen.name>
           2013 Liudas Ališauskas <liudas@akmc.lt>
           2015 Mindaugas Baranauskas <opensuse.lietuviu.kalba@gmail.com>
           2013 Chetan Khona <chetan@kompkin.com>
           2012, 2013 Bjørn Steensrud <bjornst@skogkatt.homelinux.org>
           2014 Sönke Dibbern <s_dibbern@web.de>
           2013-2018 Freek de Kruijf <freekdekruijf@kde.nl>
           2016, 2018 Karl Ove Hufthammer <karl@huftis.org>
           2014 A S Alam <aalam@users.sf.net>
           2012-2018 Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>
           2013 Marta Rybczyńska <kde-i18n@rybczynska.net>
           2018 José Nuno Coelho Pires <zepires@gmail.com>
           2012-2018 André Marcelo Alvarenga <alvarenga@kde.org>
           2012, 2013 Sergiu Bivol <sergiu@ase.md>
           2013 Yuri Efremov <yur.arh@gmail.com>
           2013 Alexander Lakhin <exclusion@gmail.com>
           2015 Alexander Potashev <aspotashev@gmail.com>
           2018 Alexander Yavorsky <kekcuha@gmail.com>
           2018 Roman Paholik <wizzardsk@gmail.om>
           2013 Jure Repinc <jlp@holodeck1.com>
           2013, 2015 Andrej Mernik <andrejm@ubuntu.si>
           2013, 2015 Chusslove Illich <caslav.ilic@gmx.net>
           2012-2018 Stefan Asserhäll <stefan.asserhall@bredband.net>
           2012 Arve Eriksson <031299870@telia.com>
           2013 Volkan Gezer <volkangezer@gmail.com>
           2013 Gheyret Kenji <gheyret@gmail.com>
           2012 Yichao Yu <yyc1992@gmail.com>
           2013, 2015 Weng Xuetian <wengxt@gmail.com>
           2012, 2013 FengChao <rainofchaos@gmail.com>
           2013, 2015 Franklin Weng <franklin at goodhorse dot idv dot tw>
           2018 pan93412 <pan93412@gmail.com>
License: GPL-2+

Files: configure-printer/Debug.cpp
       configure-printer/Debug.h
       libkcups/Debug.cpp
       libkcups/Debug.h
       po/ca/*
       po/ca@valencia/*
       po/uk/*
       print-manager-kded/Debug.cpp
       print-manager-kded/Debug.h
Copyright: 2014 Jan Grulich <jgrulich@redhat.com>
          2012-2018 Josep Ma. Ferrer <txemaq@gmail.com>
          2015, 2018 Antoni Bella Pérez <antonibella5@yahoo.com>
          2013-2018 Yuri Chornoivan <yurchor@ukr.net>
License: LGPL-2.1 or LGPL-3 or KDEeV

Files: debian/*
Copyright: 2007-2013, Debian Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
          2019 Scarlett Moore <sgmoore@kde.org>
License: GPL-2+

Files: plasmoid/package/contents/config/config.qml
       plasmoid/package/contents/ui/*
       plasmoid/package/metadata.desktop
       printer-manager-kcm/PrinterDelegate.cpp
       printer-manager-kcm/PrinterDelegate.h
Copyright: 2008-2013, Daniel Nicoletti <dantti12@gmail.com>
           2007, Ivan Cukic <ivan.cukic+kde@gmail.com>
           2014-2015, Jan Grulich <jgrulich@redhat.com>
License: LGPL-2+

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General Public
 License version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: LGPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU Library/Lesser General Public License
 version 2, or (at your option) any later version, as published by the
 Free Software Foundation
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details
 .
 You should have received a copy of the GNU Library/Lesser General Public
 License along with this program; if not, write to the
 Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 .
 On Debian systems, the complete text of the GNU Library/Lesser Public
 License version 2 can be found in "/usr/share/common-licenses/LGPL-2".

License: LGPL-2.1
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation;
 version 2.1.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 .
 On Debian systems, the complete text of the GNU Library General Public
 License version 2.1 can be found in "/usr/share/common-licenses/LGPL-2.1".

License: LGPL-3
 This package is free software; you can redistribute it and/or modify it under
 the terms of the GNU Lesser General Public License as published by the Free
 Software Foundation; either version 3 of the License.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this package; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 .
 On Debian systems, the complete text of the GNU Lesser General
 Public License can be found in `/usr/share/common-licenses/LGPL-3'.

License: KDEeV
 This file is distributed under the license LGPL either
 version 2.1 of the License, or (at your option) version 3, or any
 later version accepted by the membership of KDE e.V. (or its
 successor approved by the membership of KDE e.V.), which shall
 act as a proxy defined in Section 6 of version 3 of the license.
